package be.janolaerts.datasources;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GuestBookDao implements GuestBookDao_Interface {

    private DataSource ds;

    private static final String SQL_GET_ALL_GUESTBOOKS = "SELECT * FROM Guestbooks;";
    private static final String SQL_ADD_GUESTBOOK =
            "INSERT INTO Guestbooks (name, date, message) VALUES (?, ?, ?);";

    public GuestBookDao(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public List<GuestBook> getGuestBookItems() throws SQLException {

        List<GuestBook> guestBooks = new ArrayList<>();

        try(Connection con = ds.getConnection();
            Statement stmt = con.createStatement()) {
            try {
                ResultSet rs = stmt.executeQuery(SQL_GET_ALL_GUESTBOOKS);
                while (rs.next()) {
                    String name = rs.getString("name");
                    Date date = rs.getDate("date");
                    String message = rs.getString("message");

                    guestBooks.add(new GuestBook(name, date, message));
                }
            } catch(SQLException se) {
                System.out.println(se.getMessage());
                throw new SQLException(se);
            }

        } catch (SQLException se) {
            System.out.println(se.getMessage());
            throw new SQLException(se);
        }

        return guestBooks;
    }

    @Override
    public void addGuestBookItem(GuestBook guestBook) throws SQLException {

        Timestamp timeStamp;
        try (Connection con = ds.getConnection();
            PreparedStatement stmt = con.prepareStatement(SQL_ADD_GUESTBOOK)) {

            try {
                timeStamp = new Timestamp(guestBook.getDate().getTime());
                stmt.setString(1, guestBook.getName());
                stmt.setTimestamp(2, timeStamp);
                stmt.setString(3, guestBook.getMessage());

                stmt.executeUpdate();
            } catch (SQLException se) {
                System.out.println(se.getMessage());
                throw new SQLException(se);
            }

        } catch (SQLException se) {
            System.out.println(se.getMessage());
            throw new SQLException(se);
        }
    }
}