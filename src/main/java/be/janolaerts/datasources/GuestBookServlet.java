package be.janolaerts.datasources;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/GuestBook")
@ServletSecurity(value=@HttpConstraint(
    transportGuarantee=ServletSecurity.TransportGuarantee.CONFIDENTIAL),
    httpMethodConstraints=@HttpMethodConstraint(value="POST", rolesAllowed ="Guests"))
public class GuestBookServlet extends HttpServlet {

    @Resource(name="StudentDS")
    private DataSource ds;

    GuestBookDao guestBookDao;

    public void init() {
        guestBookDao = new GuestBookDao(ds);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            List<GuestBook> guestBookList = guestBookDao.getGuestBookItems();
            String user = request.getRemoteUser();

            request.setAttribute("guestBookList", guestBookList);
            request.setAttribute("user", user);
            request.setAttribute("request", request);

            request.getRequestDispatcher("/GuestBook.jsp")
                .forward(request, response);

        } catch (SQLException se) {
            throw new ServletException(se);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            String name = request.getParameter("name");
            String message = request.getParameter("message");
            Date now = Date.valueOf(LocalDate.now());
            guestBookDao.addGuestBookItem(new GuestBook(name, now, message));
        } catch (SQLException se) {
            throw new ServletException(se);
        }

        response.sendRedirect(request.getRequestURI());
    }
}